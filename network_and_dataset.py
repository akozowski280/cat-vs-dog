import torch
import torchvision
import torch.nn as nn
import torch.functional as F
from PIL import Image
import os

class Network(nn.Module):
    def __init__(self):
        super(Network,self).__init__()
        out1 = 12
        out2 = 32
        out3 = 64
        self.conv1 = nn.Conv2d(in_channels=3,out_channels=out1,kernel_size=5)
        self.conv2 = nn.Conv2d(in_channels=out1,out_channels=out1,kernel_size=5)
        self.conv3 = nn.Conv2d(in_channels=out1,out_channels=out2,kernel_size=3)
        self.conv4 = nn.Conv2d(in_channels=out2,out_channels=out2,kernel_size=3)
        self.conv5 = nn.Conv2d(in_channels=out2,out_channels=out3,kernel_size=3)
        self.conv6 = nn.Conv2d(in_channels=out3,out_channels=out3,kernel_size=3)

        self.lin1 = nn.Linear(in_features=out3*12*12,out_features=864)
        self.lin2 = nn.Linear(in_features=864,out_features=216)
        self.lin3 = nn.Linear(in_features=216,out_features=54)
        self.out = nn.Linear(in_features=54,out_features=2)

    def forward(self,x):
        out3 = 64
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x,kernel_size=2,stride=2)
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        x = F.max_pool2d(x,kernel_size=2,stride=2)
        x = F.relu(self.conv5(x))
        x = F.relu(self.conv6(x))
        x = F.max_pool2d(x,kernel_size=2,stride=2)

        x = F.relu(self.lin1(x.reshape(-1,out3*12*12)))
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        x = F.sigmoid(self.out(x))
        return x
    

class cats_vs_dogs_dataset(torch.utils.data.Dataset):
    def __init__(self,file_list,dir,train=True,transform=None):
        self.file_list = file_list
        self.dir = dir
        self.train = train
        self.transform = transform
        if self.train:
            if 'dog' in self.file_list[0]:
                self.label=1
            else:
                self.label=0
    
    def __len__(self):
        return len(self.file_list)
    
    def __getitem__(self, idx):
        img = Image.open(os.path.join(self.dir, self.file_list[idx]))
        if self.transform:
            img = self.transform(img)
        if self.train:
            img = img.numpy()
            return img.astype('float32'), self.label
        else:
            img = img.numpy()
            return img.astype('float32'), self.file_list[idx]